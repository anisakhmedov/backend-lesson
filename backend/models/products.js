const mongoose = require('mongoose')

const Products = mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
    },
    url: {
        type: String,
    },
    categoryId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Category",
        required: true
    },
    usersId:{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Users",
        required: true
    }

})

module.exports = mongoose.model("Products", Products)