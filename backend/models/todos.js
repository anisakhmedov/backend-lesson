const mongoose = require('mongoose')

const Todos = mongoose.Schema({
    authorId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Users",
        required: true
    },
    title: {
        type: String
    },
    description: {
        type: String
    },
    isDone: {
        type: Boolean
    },
    comments: String
})

module.exports = mongoose.model("Todos", Todos)