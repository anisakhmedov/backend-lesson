const mongoose = require('mongoose')

const Users = mongoose.Schema({
    login: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    name: { 
        type: String,
        required: false, // false
        default: "user" + Math.random().toString().slice(6)
    },
})

module.exports = mongoose.model("Users", Users)