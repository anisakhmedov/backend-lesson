
module.exports = {
    todos: require("./todos.js"),
    news: require("./news.js"),
    posts: require("./posts.js"),
    products: require("./products.js"),
    category: require("./category.js"),
    users: require("./users.js"),
}