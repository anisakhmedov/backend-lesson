const mongoose = require('mongoose')

// Ключи схемы
const Posts = mongoose.Schema({
    headCont: String,
    author: String,
    urlImg: String,
    SendAt: Date,
    isArchive: Boolean,
    published: Boolean, //Опубликовывать или нет    
    comments: [
        {
            title: String,
            user: String, // и по юзеру искать его аватар и тп...
            liked: Number,
            isLikedGeneral: Boolean, // понравилось автору
            answers: [
                {
                    ttitle: String,
                    user: String,
                    liked: Number,
                }
            ]
        }
    ],
})

module.exports = mongoose.model("Posts", Posts)