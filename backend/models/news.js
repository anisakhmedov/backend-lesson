const mongoose = require('mongoose')

// Ключи схемы
const News = mongoose.Schema({
    head: String, // Заголовок текста
    imgUrl: String,
    fake: Boolean, // ложная новость или нет
    author: String,
    description: String,
    publishedAt: Date,
    comments: [
        {
            title: String,
            autorCom: String,
            isShow: Boolean
        }
    ],
    lag: String, // en, uz, ru
    liked: Number
})

module.exports = mongoose.model("News", News)