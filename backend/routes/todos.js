const express = require("express")
const router = express.Router()
const Todos = require("../models/todos")

router.get("/", async (req, res) => {
    try {
        let todos = await Todos.find().populate("authorId")

        res.json({
            ok: true,
            message: 'Задача добавлена',
            data: todos
        })
        res.send("Salam!")
    }
    catch (error) {
        console.log(error);
    }
    console.log("Get all todos! :)");
})

router.post("/", async (req, res) => {
    try {
        Todos.create(req.body, async (error, data) => {
            if (error) {
                console.log(error);

                res.json({
                    ok: false,
                    message: "Error inside callback function WTF!",
                    error
                })
            } else {
                res.json({
                    ok: true,
                    message: "Element created!",
                    element: data
                })
            }
        })
    } catch (error) {
        console.log(error);

        res.json({
            ok: false,
            message: "Some error",
            error
        })
    }
})

router.patch("/", async (req, res) => {
})

// router.delete("/", async (req, res) => {
//     console.log('Delete element check: ' + req.body.id);

//     // 1 req.body
//     Todos.findByIdAndDelete(req.body.id, (error, data) => {
//         if (error) {

//             console.log(error);

//             res.json({
//                 ok: false,
//                 message: 'Deleting fucked!'
//             })
//         } else{
//             res.json({
//                 ok: true,
//                 message: 'Deleting successfully',
//                 data
//             })
//         }
//     })
// })

router.delete("/:id", async (req, res) => {
    console.log('Delete element check: ' + req.params.id);

    Todos.findByIdAndDelete(req.params.id, (error, data) => {
        if (error) {

            console.log(error);

            res.json({
                ok: false,
                message: 'Deleting fucked!'
            })
        } else {
            res.json({
                ok: true,
                message: 'Deleting successfully',
                data
            })
        }
    })
})


module.exports = router