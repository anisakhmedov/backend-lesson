const express = require("express")
const router = express.Router()
const News = require("../models/News")

router.get("/", async (req, res) => {
    try{
        let news = await News.find()
        
        res.json({
            ok: true,
            message: 'Задача добавлена',
            data: news
        })
        res.send("Salam!")
    }
    catch (error){
        console.log(error);
    }
    console.log("Get all news! :)");
})

router.post("/", async (req, res) => {
    // Тут идет метод создания записи в БД
    try {
        // Callback пишется при создании
        // Request.body - это объект с фронта со всеми ключами
        News.create(req.body, async (error, data) => {
            if(error) {
                console.log(error);

                res.json({
                    ok: false,
                    message: "Error inside callback function WTF!",
                    error
                })
            } else {
                res.json({
                    ok: true,
                    message: "Element created!",
                    // Значение берем из аргумента функции
                    // Тут мы получаем только что созданный элемент
                    element: data
                })
            }
        })
    } catch (error) {
        console.log(error);

        res.json({
            ok: false,
            message: "Some error",
            error
        })
    }
})

module.exports = router