const express = require("express")
const router = express.Router()
const Category = require("../models/category")

router.post("/", async (req, res) => {
    try {
        Category.create(req.body, async (error, data) => {
            if (error) {
                console.log(error);

                res.json({
                    ok: false,
                    message: "Error inside callback function WTF!",
                    error
                })
            } else {
                res.json({
                    ok: true,
                    message: "Element created!",
                    element: data
                })
            }
        })
    } catch (error) {
        console.log(error);

        res.json({
            ok: false,
            message: "Some error",
            error
        })
    }
})

module.exports = router