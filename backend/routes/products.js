const express = require("express")
const router = express.Router()
const Products = require("../models/products")

router.get("/", async (req, res) => {
    try {
        let products = await Products.find().populate("categoryId").populate("usersId")

        res.json({
            ok: true,
            message: 'Продукт добавлена',
            data: products
        })
        res.send("Salam!")
    }
    catch (error) {
        console.log(error);
    }
    console.log("Get all todos! :)");
})

router.post("/", async (req, res) => {
    try {
        let products = await Products.find().populate("categoryId").populate("usersId")

        Products.create(req.body, async (error, data) => {
            if (error) {
                console.log(error);

                res.json({
                    ok: false,
                    message: "Error inside callback function WTF!",
                    error
                })
            } else {
                res.json({
                    ok: true,
                    message: "Element created!",
                    element: products
                })
            }
        })
    } catch (error) {
        console.log(error);

        res.json({
            ok: false,
            message: "Some error",
            error
        })
    }
})

module.exports = router