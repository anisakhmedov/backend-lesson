// Импортирует наш фреймворк
const express = require("express")

// Создаем главную переменннею
const app = express()

const bodyParser = require("body-parser");
const cors = require('cors')
const mongoose = require('mongoose')
const PORT = 7777

app.use(cors())
app.use(bodyParser.json())

// Routes
app.use("/todos", require("./routes/todos.js"))
app.use("/posts", require("./routes/posts.js"))
app.use("/news", require("./routes/news.js"))
app.use("/users", require("./routes/users.js"))
app.use("/category", require("./routes/category.js"))
app.use("/products", require("./routes/products.js"))


mongoose.connect("mongodb://localhost:27017/app", () => {
    console.log("Успешно подключились к базе данных :)");
})

mongoose.connection.on('error', err => {
    console.log(err);
})

mongoose.set("debug", true)

app.get('/', (req, res) => {
    res.send("<h1 style='font-family: Gilroy;'>nothing</h1>")
})
// Слушатель по порту
app.listen(3000, () => {
    console.log(`Сервер слушает порт: ${PORT}` );
})

// Можно перейти по домену localhost:8800