let form = document.querySelector('form')
let api = 'http://localhost:3000/todos'
let modals = document.querySelector('.modals')
let main = document.querySelector('.data')
let db = []
let btn = document.querySelector('button')

const getAllData = () => {
    axios.get(api)
        .then(res => {
            db = res.data.data
            alert('get all data!')
            reload(db)
        })
        .catch(err => console.log(err))
}
getAllData()


let regexes = {
    text: /^[a-zA-Z0-9\ \']+$/
}


form.onsubmit = (e) => {
    e.preventDefault()

    let obj = {}

    let fm = new FormData(form)
    let counter = 0

    fm.forEach((value, key) => {
        obj[key] = value

        let input = form.querySelector('input[name=' + key + ']')

        let text = input.parentNode.lastElementChild

        if (input.getAttribute('data-regex')) {
            let regex = input.getAttribute('data-regex')
            if (regexes[regex].test(value)) {
                text.style.display = 'none'
                counter++
            } else {
                text.style.display = 'block'
                counter--
            }
        }
    });

    if (obj.isDone == 'on') obj.isDone = true
    else obj.isDone = false

    if (counter == form.querySelectorAll('input[data-required]').length) {
        requests('post', obj)
        for (let item of document.querySelectorAll('input')) {
            item.value = ''
        }
    }

}

const requests = (methods, data) => {
    if (methods == 'post') {
        axios.post(api, data)
            .then(res => {
                console.log(res);
                if (res.statusText == "OK") {
                    modals = modals.children[0]
                    modals.innerHTML = res.data.message
                    modals.classList.add('active')
                    btn.setAttribute('disabled', 'disabled')
                    getAllData()

                    setTimeout(() => {
                        btn.removeAttribute('disabled', 'disabled')
                        modals.classList.remove('active')
                        modals = document.querySelector('.modals')
                    }, 3000);
                } else {
                    modals = modals.children[1]
                    modals.classList.add('active')
                    modals.innerHTML = res.data.message
                    btn.setAttribute('disabled', 'disabled')
                    setTimeout(() => {
                        modals.classList.remove('active')
                        btn.removeAttribute('disabled', 'disabled')
                        modals = document.querySelector('.modals')
                    }, 3000);
                }
            })
            .catch(res => console.log(res))
    } else if (methods == 'delete') {
        console.log(123);
        axios.delete(api, data)
            .then(res => {
                console.log(res);
            })
            .catch(err => console.log(err))
    }
}

const reload = (data) => {
    main.innerHTML = ''
    for (let item of data) {
        let div = document.createElement('div')
        let head = document.createElement('div')
        let body = document.createElement('div')
        let p = document.createElement('p')
        let title = document.createElement('p')
        let description = document.createElement('p')
        let isDone = document.createElement('p')
        let del = document.createElement('div')

        del.classList.add("del")
        div.classList.add('item')
        title.classList.add('title')
        head.classList.add('head')
        body.classList.add('body')
        isDone.classList.add('isDone')

        del.innerHTML = "<p>X</p>"
        p.innerHTML = 'Комментарий: ' + item.comments
        title.innerHTML = item.title
        description.innerHTML = 'Описание: ' + item.description
        isDone.innerHTML = item.isDone

        del.onclick = () => { 
            console.log(123);
            requests('delete', item._id)
         }

        head.append(title, isDone)
        body.append(description, p)
        div.append(head, body, del)
        main.append(div)
    }
}
